use tch::{Device, Kind, nn, Reduction, Tensor};
use tch::jit::CModule;
use tch::nn::{Adam, Conv2D, ConvConfig, Module, OptimizerConfig, Path, VarStore};
use tch::vision::image;
use tch::vision::dataset::Dataset;

const IMAGE_WIDTH: i64 = 480;
const IMAGE_HEIGHT: i64 = 320;

fn load_dataset(device: Device) -> anyhow::Result<Dataset> {
	const SPLIT_RATIO: f64 = 0.85;

	let img_tensor = image::load_dir(
		"datasets/train_A",
		IMAGE_WIDTH,
		IMAGE_HEIGHT
	)?.to_kind(Kind::Float).to_grayscale() / 127.5 - 1;
	let label_tensor = image::load_dir(
		"datasets/train_B",
		IMAGE_WIDTH,
		IMAGE_HEIGHT
	)?.to_kind(Kind::Float).to_grayscale() / 127.5 - 1;
	let nb_labels: i64 = label_tensor.size()[0];

	let nb_train_images = (nb_labels as f64 * SPLIT_RATIO).floor() as i64;
	let img_tensors = img_tensor.split(nb_train_images, 0);
	let label_tensors = label_tensor.split(nb_train_images, 0);

	Ok(Dataset {
		train_images: img_tensors[0].shallow_clone(),
		train_labels: label_tensors[0].shallow_clone(),
		test_images: img_tensors[1].shallow_clone(),
		test_labels: label_tensors[1].shallow_clone(),
		labels: nb_labels
	})
}

#[derive(Debug)]
struct Net {
	layers: [Conv2D; 5]
}

impl Net {
	fn new(root: &Path) -> Self {
		let conv_config = ConvConfig {
			padding: 1,
			..ConvConfig::default()
		};

		Net { layers: [
			nn::conv2d(root / "conv1", 1, 32, 3, conv_config),
			nn::conv2d(root / "conv2", 32, 64, 3, conv_config),
			nn::conv2d(root / "conv3", 64, 64, 3, conv_config),
			nn::conv2d(root / "conv4", 64, 32, 3, conv_config),
			nn::conv2d(root / "conv5", 32, 1, 3, conv_config),
		]}
	}
}

impl Module for Net {
	fn forward(&self, xs: &Tensor) -> Tensor {
		xs.apply(&self.layers[0])
			.relu()
			.max_pool2d(&[2, 2], &[2, 2], &[0, 0], &[1, 1], false)
			.apply(&self.layers[1])
			.relu()
			.max_pool2d(&[2, 2], &[2, 2], &[0, 0], &[1, 1], false)
			.apply(&self.layers[2])
			.relu()
			.upsample()
			.apply(&self.layers[3])
			.relu()
			.upsample()
			.apply(&self.layers[4])
			.sigmoid()
	}
}

fn train(dataset: &Dataset, device: Device) -> anyhow::Result<()> {
	const NB_EPOCHS: i64 = 100;
	const BATCH_SIZE: i64 = 8;

	let mut var_store = VarStore::new(device);
	let root = var_store.root();
	let net = Net::new(&root);
	let mut opt = Adam::default().build(&var_store, 1e-3)?;
	var_store.unfreeze();

	for epoch in 1..NB_EPOCHS + 1 {
		print!("epoch {}: ", epoch);

		let mut train_loss: f64 = 0.;
		for (train_img_tensor, train_label_tensor) in dataset.train_iter(BATCH_SIZE).shuffle().to_device(device) {
			let loss = net
				.forward(&train_img_tensor)
				.mse_loss(&train_label_tensor, Reduction::Mean);
			opt.backward_step(&loss);

			train_loss += f64::from(loss);
		}
		print!("train loss = {}, ", train_loss / dataset.labels as f64);

		let _guard = tch::no_grad_guard();
		let mut test_loss: f64 = 0.;
		for (test_img_tensor, test_label_tensor) in dataset.test_iter(BATCH_SIZE).to_device(device) {
			let loss = net
				.forward(&test_img_tensor)
				.mse_loss(&test_label_tensor, Reduction::Mean);

			test_loss += f64::from(loss);
		}
		println!("test loss = {}, ", test_loss / dataset.labels as f64);
	}

	var_store.freeze();
	let mut closure = |input_tensors: &[Tensor]| {
		input_tensors.iter().map(|i| net.forward(i)).collect::<Vec<Tensor>>()
	};
	let rand_tensor = Tensor::rand(
		&[1, 1, IMAGE_HEIGHT, IMAGE_WIDTH],
		(Kind::Float, device)
	);
	let mut modl = CModule::create_by_tracing(
		"Autoencoder",
		"forward",
		&[rand_tensor],
		&mut closure
	)?;
	modl.set_eval();
	modl.save("model.pt")?;

	Ok(())
}

trait Util {
	fn to_grayscale(&self) -> Self;
	fn upsample(&self) -> Self;
}

impl Util for Tensor {
	fn to_grayscale(&self) -> Self {
		let c = self.split(1, 1);
		0.21 * &c[0] + 0.72 * &c[1] + 0.07 * &c[2]
	}

	fn upsample(&self) -> Self {
		let (_, _, h, w) = self.size4().unwrap();
		self.upsample_nearest2d(&[2 * h, 2 * w], 2.0, 2.0)
	}
}

fn main() -> anyhow::Result<()> {
	let device = Device::cuda_if_available();
	let dataset = load_dataset(device)?;

	train(&dataset, device)?;

	Ok(())
}

use tch::vision::imagenet;
#[test]
fn test_image() -> anyhow::Result<()> {
	let image = imagenet::load_image_and_resize("datasets/test_A/1.png", 480, 320)?.unsqueeze(0).to_grayscale();
	let model = CModule::load_on_device("model/model.pt", Device::Cpu)?;
	let output = model.forward_ts(&[image])?;
	let output_image = output.squeeze();
	imagenet::save_image(&output_image, "result/result.png")?;
	Ok(())
}
